package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        long temp = 1;
        if (n == 0) {
            return 1;
        }
        for (int i = n; i > 0; i--) {
            temp *= i;
        }
        return temp;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        long a1 = 1, a2 = 1, a3 = 0;
        int i = 1;
        if (n == 1 || n == 2) {
            return 1;
        }
        while (i < n - 1){
            a3 = a1 + a2;
            a1 = a2;
            a2 = a3;
            i++;
        }
        return a3;
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        StringBuilder output = new StringBuilder();
        int i;
        for (i = word.length() - 1; i >= 0; i--) {
            output.append(word.charAt(i));
        }
        return output.toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        line = line.replaceAll(" ","");
        String lineReverse = reverse(line);
        return line.equalsIgnoreCase(lineReverse) ? true : false;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        char[][] plot = new char[str1.length()][str2.length()];
        for (int i = 0; i < str1.length(); i++) {
            for (int j = 0; j < str2.length(); j++) {
                plot[i][j] = ' ';
            }
        }
        for (int i = 0; i < str1.length(); i++) {
            for (int j = 0; j < str2.length(); j++) {
                if (str1.charAt(i) == str2.charAt(j)) {
                    plot[i][j] = '*';
                }
            }
        }
        return plot;
    }
}
