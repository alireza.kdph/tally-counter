package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture6 {


    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for (int i = 0; i < arr.length; i = i + 2) {
            sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] reverseArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            reverseArr[i] = arr[(arr.length - 1) - i];
        }
        return reverseArr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        double[][] result = new double[m1.length][m2[0].length];
        if (m1.length != m2[0].length){
            throw new RuntimeException();
        }
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m2[0].length; j++) {
                for (int k = 0; k < m1[0].length; k++) {
                    result[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return result;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> result = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            List<String> row = new ArrayList<>();
            for (int j = 0; j < names[0].length; j++) {
                row.add(names[i][j]);
            }
            result.add(row);
        }
        return result;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        boolean firstTime = true;
        int i = 2;
        List<Integer> primeFacs = new ArrayList<>();
        if (n == 1) {
            return primeFacs;
        }
        do {
            if (n % i == 0) {
                n /= i;
                if (firstTime) {
                    primeFacs.add(i);
                    firstTime = false;
                }
            }
            else{
                i++;
                firstTime = true;
            }
        }while (n != 1);
        return primeFacs;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        line = line.replaceAll("(!|\\.|\\?|,)", "");
        List<String> words = Arrays.asList(line.split("\\s"));
        return words;
    }
}
