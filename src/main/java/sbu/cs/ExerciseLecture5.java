package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        Random rand = new Random();
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < length; i++) {
            password.append((char)(rand.nextInt(26) + 'a'));
        }
        return password.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if (length < 3){
            throw new IllegalValueException();
        }
        Random rand = new Random();
        StringBuilder password = new StringBuilder(weakPassword(length));
        char[] digits = {'0', '1','2','3','4','5','6','7','8','9'};
        int counter = rand.nextInt(length) + 1;
        for (int i = 0; i < counter; i++) {
            int r1 = rand.nextInt((length - 1)/2);
            int num = rand.nextInt(10);
            password.setCharAt(r1,digits[num]);
        }
        char[] special = {'?','@','!','<','>','#','%','^','(',')','%','*','_','-','.','/','='};
        int counter2 = rand.nextInt(length) + 1;
        for (int i = 0; i < counter2; i++) {
            int r1 = rand.nextInt((length - 1)/2) + (length - 1)/2;
            int random = rand.nextInt(17);
            password.setCharAt(r1,special[random]);
        }
        return password.toString();

    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        int i = 0;
        while (fibonacci(++i) <= n){
            int fib = fibonacci(i);
            int toBinFib = binarySum(fib);
            if (toBinFib + fib == n) {
                return true;
            }
        }
        return false;
    }
    public int fibonacci(int n) {
        int a1 = 1, a2 = 1, a3 = 0;
        int i = 1;
        if (n == 1 || n == 2) {
            return 1;
        }
        while (i < n - 1){
            a3 = a1 + a2;
            a1 = a2;
            a2 = a3;
            i++;
        }
        return a3;
    }
    public int binarySum (int n){
        int[] binary = new int[30];
        int i = 0;
        while (n > 0){
            binary[i++] = n % 2;
            n /= 2;
        }
        int sum = 0;
        for (int j = 0; j < binary.length; j++) {
            sum += binary[j];
        }
        return sum;
    }
}
